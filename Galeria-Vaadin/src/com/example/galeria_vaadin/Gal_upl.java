package com.example.galeria_vaadin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Image;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

@SuppressWarnings("serial")
public class Gal_upl implements Receiver, SucceededListener {
	
	    public File file;
		public String  basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
		FileResource resource;

		public OutputStream receiveUpload(String filename,
	                                      String mimeType) {
	        // Create upload stream
	        FileOutputStream fos = null; // Stream to write to
	        try {
	            // Open the file for writing.
	            file = new File(basepath + "/images/" + filename);
	            fos = new FileOutputStream(file);
	            if (filename.endsWith(".jpg") || filename.endsWith(".png") || filename.endsWith(".jpeg") || filename.endsWith(".gif")){
	            				FileResource resource = new FileResource(file);
	            				Image img = new Image("", resource);
	            				((GaleriaUI)UI.getCurrent()).getImages().add(img);
	            				Notification.show("Zdjecie dodano do Galeri!",  Type.WARNING_MESSAGE);
	            }
	            
	            else   Notification.show("To nie jest zdjecie", Type.ERROR_MESSAGE);
	            
	        } catch (final java.io.FileNotFoundException e) {
	            new Notification("Could not open file<br/>",
	                             e.getMessage(),
	                             Notification.Type.ERROR_MESSAGE)
	                .show(Page.getCurrent());
	            return null;
	        }
	        return fos; // Return the output stream to write to
	    }

		@Override
		public void uploadSucceeded(SucceededEvent event) {
			// TODO Auto-generated method stub
			
		}

}
