package com.example.galeria_vaadin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.vaadin.data.Property;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;

public class Gal_kom {
	private Table komenty = new Table("Komentarze:");	
	private TextField kom_tresc = new TextField("Tre��:");
	private TextField kom_autor = new TextField("Autor:");
	private Button dodaj_k = new Button();
	private HashMap<Integer, ArrayList<String>> koment_map_a = new HashMap<Integer, ArrayList<String>>();	
	private HashMap<Integer, ArrayList<String>> koment_map_t = new HashMap<Integer, ArrayList<String>>();
	private int i;
	private Label current = new Label("Wybrany komentarz: ");
	Button usun_k = new Button("Usu� wybrany komentarz");
	
	Gal_kom ()
	{
		komenty.setWidth("55%");	
		komenty.setHeight("60px");
		komenty.addContainerProperty("ID",       Integer.class, null);
		komenty.addContainerProperty("Autor", String.class,  null);
		komenty.addContainerProperty("Tre��",  String.class,  null);	
		komenty.setSelectable(true);
		komenty.setImmediate(true);
		komenty.addValueChangeListener(new Property.ValueChangeListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(
					com.vaadin.data.Property.ValueChangeEvent event) {
				current.setValue("Wybrany komentarz: " + komenty.getValue());
				
			}
		});
		kom_tresc.setWidth("180px");
		kom_autor.setWidth("130px");
		addEventForSendButton();
		addEventForUsunButton();
	}
	
	public Button getSendButton()
	{
		return this.dodaj_k;
	}

	private void addEventForSendButton() {
		
		getSendButton().addClickListener(new Button.ClickListener() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				dodaj_komentarz(i);
			}
		});
		
	}
	
private void addEventForUsunButton() {
		
		getUsun_k().addClickListener(new Button.ClickListener() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				String s;
				String s2[] = null;
				s= komenty.getItem(komenty.getValue()).toString();
				komenty.removeItem(komenty.getValue());
				s2 = s.split(" ", 3);
				for (Map.Entry<Integer, ArrayList<String>> entry : koment_map_a.entrySet()) {
					ArrayList<String> x = 	entry.getValue();
						for(int i = 0; i<=x.size()-1; i++){
							if(x.get(i).equals(s2[1])){
								koment_map_a.get(entry.getKey()).remove(i);
								}
						}
				}
				for (Map.Entry<Integer, ArrayList<String>> entry : koment_map_t.entrySet()) {
					ArrayList<String> x = 	entry.getValue();
						for(int i = 0; i<=x.size()-1; i++){
							if(x.get(i).equals(s2[2])){
								koment_map_t.get(entry.getKey()).remove(i);
								}
						}
				}
			}
		});
		
	}
	 void dodaj_komentarz(int i) {
		 try {
	            kom_autor.validate();
	        } catch (InvalidValueException e) {
	            Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
	            kom_autor.setValidationVisible(true);
	            return;
	        }
		 try {
	            kom_tresc.validate();
	        } catch (InvalidValueException e) {
	            Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
	            kom_tresc.setValidationVisible(true);
	            return;
	        }
		String tresc = kom_tresc.getValue();
		String autor = kom_autor.getValue();
		kom_tresc.setValue("");
		kom_autor.setValue("");
		if (tresc != "")
		{
			zapisz_k(i, autor, tresc);
			initKoment(i);
	}
}
	 
	public Table getTextArea()
	{
		return komenty;
	}
	public void zapisz_k(int i, String autor, String tresc)
	{
		  if (wczytaj_k_a(i) != null) {
			 	koment_map_a.get(i).add(autor);	
			 	koment_map_t.get(i).add(tresc);
		 }
		 else{
			 	ArrayList<String> a = new ArrayList<String>();
			 	ArrayList<String> t = new ArrayList<String>();
			 	a.add(autor);
			 	t.add(tresc);
			 	koment_map_a.put(i, a);	 
			 	koment_map_t.put(i, t);
			 }
		  
	}
	
	public ArrayList<String> wczytaj_k_a(int i)
	{
		return koment_map_a.get(i);
	}
	
	public ArrayList<String> wczytaj_k_t(int i)
	{
		return koment_map_t.get(i);
	}

	public TextField getKom_tresc() {
		return kom_tresc;
	}
	public TextField getKom_autor() {
		return kom_autor;
	}
	
	public Label getCurrentL() {
		return current;
	}
	
	public Button getUsun_k() {
		return usun_k;
	}


	public void setKom_tresc(TextField kom_tresc) {
		this.kom_tresc = kom_tresc;
	}

	public void setIndex(int s) {
		this.i = s;
		
	}
	public void initKoment(int i)
	{
	 if (wczytaj_k_t(i) != null) {
		 getTextArea().removeAllItems();
		 for(int x=0;x<=wczytaj_k_t(i).size()-1;x++){
			 	getTextArea().addItem(new Object[] { x, wczytaj_k_a(i).get(x),  wczytaj_k_t(i).get(x)}, new Integer(x));
		 }
	 }
	else getTextArea().removeAllItems();
	}
}


