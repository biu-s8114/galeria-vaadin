package com.example.galeria_vaadin;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import com.google.gwt.event.dom.client.ClickHandler;
import com.vaadin.annotations.Theme;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.event.MouseEvents;
import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.event.MouseEvents.ClickListener;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("galeria_vaadin")
public class GaleriaUI extends UI {
	
	String basepath = new String();
	ArrayList<Image> images = new ArrayList<Image>();
	final GridLayout grid = new GridLayout(3, 3);
	HorizontalLayout img_panel = new HorizontalLayout();
	int img_i = 0;
	Gal_kom kom = new Gal_kom();
	Gal_zoom z;
	File webinfFolder;

		public void init(VaadinRequest request) {
			    setContent(grid);
			    grid.setWidth("100%");							
				basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
				initImages();
				displayImages(img_i);
				grid.addComponent(img_panel, 1, 0);
				grid.setComponentAlignment(img_panel, Alignment.TOP_CENTER);
				final Button start_s = new Button("Rozpocznij pokaz slajd�w");
								
				VerticalLayout kom_panel = new VerticalLayout();
				HorizontalLayout kom_panel1 = new HorizontalLayout();
				kom_panel1.setWidth("55%");
				kom_panel.setMargin(true);		
				kom_panel.addComponent(start_s);
				kom_panel.setComponentAlignment(start_s, Alignment.TOP_CENTER);
				kom_panel.addComponent(kom.getTextArea());
				kom_panel.setComponentAlignment(kom.getTextArea(), Alignment.TOP_CENTER);
				kom.getKom_autor().addValidator(new StringLengthValidator("Autor - Wymagane od 3 do 10 znak�w", 3, 10, true));
				kom.getKom_autor().setValidationVisible(false);			
				kom_panel1.addComponent(kom.getKom_autor());
				kom.getKom_tresc().setWidth("155px");
				kom.getKom_tresc().addValidator(new StringLengthValidator("Tre�� komentarza - Wymagane conajmniej 2 znaki", 2, 100, true));
				kom.getKom_tresc().setValidationVisible(false);
				kom_panel1.addComponent(kom.getKom_tresc());
				kom.getSendButton().setWidth("110px");
				kom.getSendButton().setCaption("Skomentuj");
				kom_panel1.addComponent(kom.getSendButton());
				kom_panel1.setComponentAlignment(kom.getSendButton(), Alignment.BOTTOM_RIGHT);
				kom_panel.addComponent(kom_panel1);
				kom_panel.setComponentAlignment(kom_panel1, Alignment.MIDDLE_CENTER);
				
     			grid.addComponent(kom_panel,1,1);				
			    grid.setComponentAlignment(kom_panel, Alignment.MIDDLE_CENTER);
			    
				final Button btLeft = new Button("Poprzedni");
				final Button btFirst = new Button("Pierwszy");
				final Button btRandom = new Button("Losowy");
				final Button btLast = new Button("Ostatni");
				final Button btRight = new Button("Nastepny");
				final HorizontalLayout button_panel = new HorizontalLayout();

				button_panel.setWidth("55%");
				button_panel.addComponent(btFirst);
				button_panel.addComponent(btLeft);
				button_panel.addComponent(btRandom);
				button_panel.setComponentAlignment(btRandom, Alignment.MIDDLE_CENTER);
				button_panel.addComponent(btRight);
				button_panel.addComponent(btLast);
	
				grid.addComponent(button_panel,1,2);
				grid.setComponentAlignment(button_panel, Alignment.BOTTOM_CENTER);
				
				final Label log = new Label("Zaloguj si�, aby doda� zdj�cie.");
				final TextField user = new TextField("Login: ");
				final PasswordField pass = new PasswordField("Has�o: ");
				final Button login = new Button("Zaloguj");		
				final Button usun_img = new Button("Usu� to zdj�cie!");	
				final VerticalLayout login_panel = new VerticalLayout();
				final VerticalLayout admin_panel = new VerticalLayout();
				
				login_panel.addComponent(log);
				login_panel.addComponent(user);
				login_panel.addComponent(pass);
				login_panel.addComponent(login);
				login_panel.setMargin(true);
				grid.addComponent(login_panel,0,0);
				
				Gal_upl receiver = new Gal_upl();
				final Upload upload = new Upload("Wybierz zdj�cie do dodania", receiver);
				admin_panel.addComponent(upload);
				
				admin_panel.setMargin(true);
				admin_panel.addComponent(new Label("Panel usuwania komentarzy:"));
				admin_panel.addComponent(kom.getCurrentL());
				admin_panel.addComponent(kom.getUsun_k());
				admin_panel.addComponent(new Label("Panel usuwania zdj��:"));
				admin_panel.addComponent(usun_img);
				admin_panel.setVisible(false);
				login_panel.addComponent(admin_panel);
				
				btFirst.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						img_i = 0;
						displayImages(img_i);
					}
				});
				btLast.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						img_i=images.size()-1;
						displayImages(img_i);
					}
				});
				btRandom.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						Random generator = new Random();
						img_i=generator.nextInt(images.size()-1);
						displayImages(img_i);
					}
				});
				
				btLeft.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						img_i--;
						if (img_i<0) img_i=images.size()-1;
						displayImages(img_i);						
					}
				});
				
				btRight.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						img_i++;
						if (img_i>images.size()-1) img_i=0;
						displayImages(img_i);
					}
				});
				
				login.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						if(user.getValue().equals("admin") && pass.getValue().equals("1234")){
							admin_panel.setVisible(true);
							log.setValue("Witaj Adminie!");
							user.setVisible(false);
							pass.setVisible(false);
							login.setVisible(false);
						}
						else{
							Notification.show("B��dny login lub has�o", Type.ERROR_MESSAGE);
						}
						}
					});	
				start_s.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						new Gal_slide().start();
						UI.getCurrent().setPollInterval(500);
					}
				});
				usun_img.addClickListener(new Button.ClickListener() {
					public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
						if (images.size() > 0) 
							{
							images.remove(img_i);
							if (img_i == images.size()) img_i--;
							displayImages(img_i);
							}
						else Notification.show("Brak zdj�c!");
					}
				});
				}	
				
		public void initImages() {
			    for (int i = 1 ; i<=10  ; i++){
			        FileResource resource = new FileResource(new File(basepath +
			            "/images/" + i +".jpg"));
			        images.add(new Image("",resource));
			    }   
			}
		public ArrayList<Image> getImages() {
			return images;
		}

		public void setImages(ArrayList<Image> images) {
			this.images = images;
		}
		
		public void addEventGaleriePictures(final int img)
		{
			ClickListener x = new ClickListener() {
				
				@Override
				public void click(ClickEvent event) {	
					z = new Gal_zoom(images.get(img));		
					addWindow(z);
					}
			};
			images.get(img).addClickListener(x); 
			}	
		public void addImges(String i) {
		       FileResource resource = new FileResource(new File(i));
		       Image img = new Image("", resource);
		       ((GaleriaUI)UI.getCurrent()).getImages().add(img);
		    }
		public void displayImages(int img){
			img_panel.removeAllComponents();
        	Image i = images.get(img);
        	i.setWidth("400px");
        	i.setHeight("400px");
			img_panel.addComponent(i);
			kom.setIndex(img);
			kom.initKoment(img);
			addEventGaleriePictures(img);
		}
		
}


