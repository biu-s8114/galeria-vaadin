package com.example.galeria_vaadin;

import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.event.MouseEvents.ClickListener;
import com.vaadin.ui.Image;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

//Define a sub-window by inheritance
class Gal_zoom extends Window {
 public Gal_zoom(Image img) {
     super("Powiekszone zdj�cie"); // Set window caption
     center();

     // Some basic content for the window
     VerticalLayout content = new VerticalLayout();
     img.setWidth("500px");
     img.setHeight("500px");
    img.addClickListener(new ClickListener() {
			
			public void click(ClickEvent event) {
				
				((GaleriaUI)UI.getCurrent()).z.close();
				((GaleriaUI)UI.getCurrent()).displayImages(((GaleriaUI)UI.getCurrent()).img_i);
				setVisible(false);
			}
		});
     content.addComponent(img);
     content.setMargin(true);
     setContent(content);
     
     // Disable the close button
     setClosable(false);
 }
}